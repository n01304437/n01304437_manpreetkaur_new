﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Assignment1.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Assignment_1</title>
    
</head>
<body>
    <form id="form1" runat="server">
        <h1>Hotel Booking Reservation</h1>
        <div>
            <asp:Panel ID="Panel1" runat="server" GroupingText="Client Information" Width="470px">
                <table class="auto-style1">
                    <tr>
                        <td class="auto-style12">Name</td>
                        <td class="auto-style23">
                            <asp:TextBox ID="Name" runat="server" Width="157px"></asp:TextBox>
                        </td>
                        <td class="auto-style11">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="Name" ErrorMessage="Please enter a name" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style12">Gender:</td>
                        <td class="auto-style23">
                            <asp:RadioButton ID="M" runat="server" GroupName="Gender" />
                            Male
                            <asp:RadioButton ID="F" runat="server" GroupName="Gender" />
                            &nbsp;Female</td>
                        <td class="auto-style11">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style13">Address</td>
                        <td class="auto-style24">
                            <asp:TextBox ID="Address" runat="server" Width="156px" OnTextChanged="TextBox2_TextChanged"></asp:TextBox>
                        </td>
                        <td class="auto-style7">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="Address" ErrorMessage="Enter Address" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style12">Phone</td>
                        <td class="auto-style23">
                            <asp:TextBox ID="phone" runat="server" Width="155px"></asp:TextBox>
                        </td>
                        <td class="auto-style11">
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="phone" ErrorMessage="Invalid Phone number" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}" ForeColor="Red"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style12">Postal Code</td>
                        <td class="auto-style23">
                            <asp:TextBox ID="Postel_code" runat="server" Width="159px"></asp:TextBox>
                        </td>
                        <td class="auto-style11">
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="Postel_code" ErrorMessage="Invalid postal code" ValidationExpression="\d{5}(-\d{4})?" ForeColor="Red"></asp:RegularExpressionValidator>
                        </td>
                        
                    </tr>
                    <tr>
                        <td class="auto-style13">Email</td>
                        <td class="auto-style24">
                            <asp:TextBox ID="email" runat="server" Width="158px"></asp:TextBox>
                        </td>
                        <td class="auto-style7">
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="email" ErrorMessage="Invalid id" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="Red"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style13" colspan="3">
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ForeColor="Red" />
                        </td>
                    </tr>
                    <caption>
                        <br />
                    </caption>
                    </tr>
                </table>
            </asp:Panel>
            <br />
            <asp:Panel ID="Panel2" runat="server" GroupingText="Room Information" Width="469px">
                <table class="auto-style25">
                    <tr>
                        <td class="auto-style26">Number of person:</td>
                        <td class="auto-style31">
                            <asp:DropDownList ID="DropDownList1" runat="server">
                                <asp:ListItem>1-5</asp:ListItem>
                                <asp:ListItem>6-10</asp:ListItem>
                                <asp:ListItem>11-15</asp:ListItem>
                                <asp:ListItem>16-20</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="auto-style30">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style26">Room required:</td>
                        <td class="auto-style31">
                            <asp:DropDownList ID="DropDownList2" runat="server">
                                <asp:ListItem>1</asp:ListItem>
                                <asp:ListItem>2</asp:ListItem>
                                <asp:ListItem>3</asp:ListItem>
                                <asp:ListItem>4</asp:ListItem>
                                <asp:ListItem>5</asp:ListItem>
                                <asp:ListItem>6</asp:ListItem>
                                <asp:ListItem>7</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="auto-style30">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style26">Size of room:</td>
                        <td class="auto-style31">
                            <asp:CheckBox ID="CheckBox1" runat="server" Text="Small" />
                            &nbsp;<asp:CheckBox ID="CheckBox2" runat="server" Text="Medium" />
                            &nbsp;<asp:CheckBox ID="CheckBox3" runat="server" Text="Large" />
                        </td>
                        <td class="auto-style30">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style26">Price:</td>
                        <td class="auto-style31">
                            <asp:TextBox ID="Price" runat="server"></asp:TextBox>
                        </td>
                        <td class="auto-style30">
                            <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="Price" ErrorMessage="Out of range" MaximumValue="15000" MinimumValue="1000" ForeColor="Red"></asp:RangeValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style26">Traveling purpose:</td>
                        <td class="auto-style31">
                            <asp:DropDownList ID="DropDownList3" runat="server">
                                <asp:ListItem>Holidays</asp:ListItem>
                                <asp:ListItem>Business</asp:ListItem>
                                <asp:ListItem>Marriage Function</asp:ListItem>
                                <asp:ListItem>More</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="auto-style30">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style26">Date</td>
                        <td class="auto-style31">
                            <asp:TextBox ID="Date" runat="server"></asp:TextBox>
                            <br />
                        </td>
                        <td class="auto-style30">
                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="Date" ErrorMessage="Invalid date" Operator="DataTypeCheck" ForeColor="Red"></asp:CompareValidator>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <br />
            <asp:Button ID="Button1" runat="server" Text="Button" />
            <br />
            
        </div>
    </form>
</body>
</html>
